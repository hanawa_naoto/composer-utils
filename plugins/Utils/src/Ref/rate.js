let order_id = "%ORDER_ID%";
let img = "%IMG%";
let rate = (order_id,score) => {
    this.get("orders/rate/"+order_id+"/"+score).toPromise().then(_=>{
        this.prompt.askResult("Please leave a comment about your FavorPls rider (Optional)","Rider Feedback",false,null,"e.g The rider was nice and courteous").then(comment => {
            console.log(comment);
            this.prompt.alert("Your feedback has been submitted successfully. Thank you for using FavorPls!!","Feedback Submitted");    
        }).catch( e => {
            console.log("There was an error",e);
            this.prompt.alert("Your feedback has been submitted successfully. Thank you for using FavorPls!!","Feedback Submitted");
        });
        
    });
};
let rates = {
    "Very Satisfied" : () => {
        rate(order_id,2);
    },
    "Satisfied" : () => {
        rate(order_id,1);
    },
    "Okay" : () => {
        rate(order_id,0);
    },
    "Bad" : () => {
        rate(order_id,-1);
    },
    "Very Bad" : ()=> {
        rate(order_id,-2);
    }
    
};
this.prompt.selectAlert(Object.keys(rates),"How would you rate the service of your FavorPls rider today?<br><br><center><img width='50%' src='"+img+"' /></center>").then(rate =>{
    console.log(rate,"Rest");
    rates[rate[0].value]();
});
